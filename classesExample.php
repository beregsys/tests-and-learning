<?php

interface Human {
    public function getHeight() : string;
    public function getWeight() : string;
}

class Person {
    public function getFullName() : string
    {
        return "{$this->name} {$this->surname}";
    }
}

trait HasSkill {
    public function getSkill()
    {
        return $this->skill;
    }
}

class Employee extends Person implements Human {

    use HasSkill;

    public string $name;
    public string $surname;
    private int $salary;
    private int $height;
    private int $weight;
    private string $skill;

    public function __construct(string $name, string $surname, int $salary, int $height, int $weight, string $skill = '')
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->salary = $salary;
        $this->height = $height;
        $this->weight = $weight;
        $this->skill = $skill;
    }
    
    public function getHeight() : string
    {
        return $this->height . ' sm';
    }

    public function getWeight() : string
    {
        return $this->weight . ' kg';
    }

    public function getBrutto() : string
    {
        return $this->salary * 1.23 . ' zl';
    }
    
}

//Data from DB

$employeesFromDb = [
    [
        'name' => 'Sviatoslav',
        'surname' => 'Berezhnyi',
        'salary' => 6000,
        'height' => 185,
        'weight' => 80,
        'skill' => 'Programming'
    ],
    [
        'name' => 'Lidiia',
        'surname' => 'Berezhna',
        'salary' => 7000,
        'height' => 158,
        'weight' => 53
    ],
];

$employees = [];

foreach($employeesFromDb as $person) {
    $employees[] = new Employee($person['name'], $person['surname'], $person['salary'], $person['height'], $person['weight'], $person['skill'] ?? '');
}

foreach( $employees as $employee ) {
    echo "<div>Employee Name: {$employee->getFullName()}</div>";
    echo "<div>Salary: {$employee->getBrutto()}</div>";
    echo "<div>Height: {$employee->getHeight()}</div>";
    echo "<div>Weight: {$employee->getWeight()}</div>";
    echo $employee->getSkill() ? "<div>Skill: {$employee->getSkill()}</div>" : '';
    echo "<hr>";
}
